<?php
/**
 * Created by PhpStorm.
 * User: jv
 * Date: 30/04/2018
 * Time: 14:13
 */

class education
{
    public $years;
    public $institution;
    public $education;


    function __construct($years, $institution, $education)
    {
        $this->years = $years;
        $this->institution = $institution;
        $this->education = $education;
    }
}