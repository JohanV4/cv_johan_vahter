<?php
/**
 * Created by PhpStorm.
 * User: jv
 * Date: 03/04/2018
 * Time: 18:20
 */
require_once ("lib/tpl.php");
require_once ("items_from_sqlite.php");
require_once ("education.php");
require_once ("work.php");
require_once ("email.php");
require_once ("index.php");

libxml_use_internal_errors(true);
$cmd = isset($_GET["cmd"]) ? $_GET["cmd"] : "view";
$data = [];

if ($cmd == "view") {
    $data['$heading'] = 'Andmed';
    $data['$content'] = 'tpl/data.html';
   }

else if ($cmd == "education"){
    $entryList = get_education_info();
    $data['$heading'] = 'Haridus';
    $data['$info'] = $entryList;
    $data['$content'] = 'tpl/educationlist.html';
    }

else if ($cmd == "job"){
    $data['$heading'] = 'Töökogemus';
    $entryList = get_work_info();
    $data['$info'] = $entryList;
    $data['$content'] = 'tpl/worklist.html';
    }

else if ($cmd == "skills"){
    $data['$heading'] = 'Oskused';
    $data['$content'] = 'tpl/skills.html';
    }

else if ($cmd == "about"){
    $data['$heading'] = 'Endast';
    $text = get_file_data();
    $data['$info'] = $text;
    $data['$content'] = 'tpl/about.html';}


else if ($cmd == "contact"){
    $data['$heading'] = 'Kontakt';
    $data['$content'] = 'tpl/contact.html';
    }

else if ($cmd = "send"){
    sendEmail($_POST['name'], $_POST['phone'], $_POST['email'], $_POST['message']);
    header("Location: ?cmd=contact");
}
    print render_template("tpl/main.html", $data);