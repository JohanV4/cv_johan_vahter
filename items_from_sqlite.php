<?php
/**
 * Created by PhpStorm.
 * User: jv
 * Date: 30/04/2018
 * Time: 14:11
 */
//require_once("about.txt");

function get_education_info() {
    $connection = new PDO('sqlite:data.sqlite');
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $connection->prepare("select * FROM educationInfo");
    $stmt->execute();

    $educationList = [];
    foreach ($stmt as $row) {
        $education = new education($row["schoolyear"], $row["institution"], $row["education"]);
        $educationList[] = $education;
    }
    return $educationList;
}

function get_work_info() {
    $connection = new PDO('sqlite:data.sqlite');
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $connection->prepare("select * FROM workInfo");
    $stmt->execute();

    $workList = [];
    foreach ($stmt as $row) {
        $work = new work($row["workyear"], $row["company"], $row['job'], $row["work"]);
        $workList[] = $work;
    }
    return $workList;
}

function get_file_data(){
    $lines = explode("\n", file_get_contents('about'));
    return $lines;
}