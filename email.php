<?php
/**
 * Created by PhpStorm.
 * User: jv
 * Date: 30/04/2018
 * Time: 16:47
 */
function sendEmail($name, $phone, $visitor_email, $message) {
    $email_subject = "Vormi sisestus";
    $message = wordwrap($message, 70);
    $email_body = "Saabus sõnum kasutajalt $name.\n".
              "Sõnumi sisu:\n $message \n".
                "$name number on: $phone";
    $to = 'johan.vahter@gmail.com';
    $headers = 'From: johan.vahter@gmail.com' . "\n" .
        'Reply-To: '. $visitor_email . "\n".
    'X-Mailer: PHP/'. phpversion();

    mail($to, $email_subject, $email_body, $headers);
}